// Copyright 2015 cFront Software Ltd, http://www.cfront.co.uk
// Free for any use, provided this copyright notice is maintained.
if(typeof(cFront) === 'undefined') var cFront = {};
if(typeof(cFront.Drawing) === 'undefined') cFront.Drawing = {};

/*
	World engine.
	
	API: 
	
	new(canvasel, options);
	
	.run()
	.end()
	.addActor()
	
	.width()        - Get canvas width
	.height()       - Get canvas height
	
	Options:
	
	autoRun         - Default(false) - Start immediately after creation
	clearEveryFrame - Default(true)  - Empty canvas every frame. Turn off for weird effects.
	fitCanvas       - Default(true)  - Resize the canvas to fit its parent when the world is initialized. Useful as you can't size canvas in CSS
	onDrawing       - Callback - world.onDrawing(frame, t) {} - Called before the actors are drawn with frame and current world time.
	
	EXAMPLE:
	
	Creates a new world and runs it. New snot drips are added at random multiples of the current frame and initialized with random values.
	
	var world = new cFront.Drawing.World(document.getElementById('c'), {
    autoRun: true,
    onDrawing:
        function(frame, t)
        {
            if(frame % Math.round(10 + (Math.random() * 60)) == 0)
            {
                world.addActor(new cFront.Drawing.SnotActor({
                    x: Math.random() * this.width(),
                    size: Math.random() * 5
                }));
            }
        }
	});	

*/
cFront.Drawing.World = function(c, options)
{
    var _this = this;
    var _frame;
    var _start;
    var _delta;
    var _animid;
    var _running = false;
    var _actors = [];

    this.options = options || {};    
    this.options.clearEveryFrame = this.options.clearEveryFrame !== false;
    this.options.fitCanvas = this.options.fitCanvas !== false;
    
    this.canvas = c;
    this.ctx = c.getContext("2d");    
    
    this.init = function()
    {
        if(this.options.fitCanvas)
        {            
            var w, h;
            
            if(c.parentNode.nodeName == 'BODY')
            {
                w = window.innerWidth;
                h = window.innerHeight;
            }
            else
            {
                w = c.parentNode.offsetWidth;
                h = c.parentNode.offsetHeight;
            }
            
            c.width = w;
            c.height = h;
        }
    };
    
    this.run = function()
    {
        _running = true;
        
        _start = new Date().getTime();
        _delta = 0;
        _frame = 0;     
        
        for(var i = 0; i < _actors.length; i++)
            actors[i].init();            
        
        _animid = requestAnimationFrame(_drawFrame);
    };
    
    this.end = function()
    {
        _running = false;
        _animid && cancelAnimationFrame(_animid);
    };
    
    this.addActor = function(actor)
    {
        _actors.push(actor);
        
        if(_running)
            actor.init();
    };
    
    this.width = function()
    {
        return this.canvas.width;
    }

    this.height = function()
    {
        return this.canvas.height;
    }
    
    var _drawFrame = function()
    {        
        if(!_running)
            return;              
        
        var w = _this.canvas.width;
        var h = _this.canvas.height;
        
        if(_this.options.clearEveryFrame)
            _this.ctx.clearRect(0, 0, w, h);
        
        _frame++;
        _delta = new Date().getTime() - _start;
        
        _this.options.onDrawing && _this.options.onDrawing.call(_this, _frame, _delta);
        
        for(var i = 0; i < _actors.length; i++)
        {
            if(_running && !_actors[i].expired())
                _actors[i].draw(_this.ctx, _delta, w, h, _this);
            else if(!_running)
                break;
        }
                
        requestAnimationFrame(_drawFrame);
    };
    
    this.init();
    
    if(options && options.autoRun)
        this.run();
};

// =====================================================================================================================================================================
/* 
	Actor base class. Override .draw() to generate your world.
	
	draw() parameters:
	
	ctx    - Drawing context
	t      - World time
	w      - Canvas width
	h      - Canvas height
	world  - world object
	
	Within draw() it is normal to call this.t() to get actor local time and this.expired(true) when the actor is no longer required.
	
	API:
	
	.id()      - Get / set actor id (NOT CURRENTLY USED)
	.expired() - Called .expired(true) to remove from drawing activities
	.t()       - Get actor local time
	
	.init()    - Called by the world when the world is started; or when the actor is added if the world is running.
	.draw()    - Called by the world to draw the actor; see above for parameters.
*/
cFront.Drawing.Actor = function()
{
};

cFront.Drawing.Actor.prototype.id = function()
{
    if(arguments.length > 0)
        this._id = arguments[0];
    
    return this._id;
};

cFront.Drawing.Actor.prototype.expired = function(v)
{
    if(v === true)
        this._expired = true;
    
    return this._expired;
};

cFront.Drawing.Actor.prototype.t = function()
{
    return new Date().getTime() - this._start;
};

cFront.Drawing.Actor.prototype.init = function()
{
    this._id = '-1';
    this._expired = false;
    this._start = new Date().getTime();
};       

cFront.Drawing.Actor.prototype.draw = function(ctx, t, w, h, world)
{
};       