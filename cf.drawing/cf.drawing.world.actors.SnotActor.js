// Copyright 2015 cFront Software Ltd, http://www.cfront.co.uk
// Free for any use, provided this copyright notice is maintained.
/*
	Draw a purple runny snot drip.
*/
cFront.Drawing.SnotActor = function(options)
{
    this.options = options;
    this.opacity = 0.9;
};

cFront.Drawing.SnotActor.prototype = new cFront.Drawing.Actor;

cFront.Drawing.SnotActor.prototype.draw = 
	function(ctx, t, w, h, world)
	{
		var size = this.options && this.options.size ? this.options.size : 5;
		var x = this.options && this.options.x ? this.options.x : w / 2;
		var y = (this.t() / (5 + (3 / size))) - (size * 40);
						
		// If drop is off bottom of canvas, then fade it out and then expire it.
		if(y > (h + (size * 4)))
		{
			y = (h + (size * 4));
			
			this.opacity -= 0.01;
			
			if(this.opacity <= 0)
			{
				this.opacity = 0; // IE flashes without this
				this.expired(true);
			}
		}
		//
						
		var m = size;
			 
		ctx.save();
		ctx.fillStyle = 'rgba(87, 0, 181, ' + this.opacity + ')';
		ctx.strokeStyle = 'rgba(87, 0, 181, ' + this.opacity + ')';
		ctx.lineWidth = size * 3;
		ctx.translate(x, y); 
		
		// Draw drop (using shape code from http://hernan.amiune.com/labs/particle-system/hello-world.html)
		ctx.beginPath();    
		ctx.moveTo(0,0);
		ctx.bezierCurveTo(0*m,5*m,0*m,10*m,5*m,15*m);   
		ctx.bezierCurveTo(10*m,20*m,12*m,26*m,10*m,30*m);   
		ctx.bezierCurveTo(6*m,40*m,-6*m,40*m,-10*m,30*m);
		ctx.bezierCurveTo(-12*m,26*m,-10*m,20*m,-5*m,15*m);
		ctx.bezierCurveTo(0*m,10*m,0*m,5*m,0*m,0*m);
		ctx.fill();	
		
		// Draw trail
		ctx.moveTo(0,0);
		ctx.lineTo(0, -y);
		ctx.stroke();

		// Draw highlight
		ctx.beginPath();  
		ctx.fillStyle = 'rgba(255, 255, 255, ' + this.opacity + ')';
		ctx.arc(-5*m, 25*m, size * 3, 0, 2 * Math.PI);
		ctx.fill();
		
		ctx.restore();
	};